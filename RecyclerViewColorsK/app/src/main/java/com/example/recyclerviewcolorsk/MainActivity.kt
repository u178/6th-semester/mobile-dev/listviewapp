package com.example.recyclerviewcolorsk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    val listSize = 15
    val list = arrayListOf<String>("Mars", "Venus", "Earth", "Pluto", "Jupyter", "Titan")
    var generatedList = genList(list, listSize)
    lateinit var adapter: ArrayAdapter<String>
    lateinit var lv: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, this.generatedList)
        this.lv = findViewById<ListView>(R.id.list)
        this.lv.adapter = adapter
    }

    private fun genList(original: ArrayList<String>, len: Int) : ArrayList<String> {
        val list = ArrayList<String>()
        for (i in 0..len) {
            list.add(original[Random.nextInt(original.size)])
        }

        return list
    }

    fun updateAdapter(view: android.view.View) {
        this.generatedList.clear()
        this.generatedList.addAll(this.genList(this.list, this.listSize))
//        generatedList = this.genList(this.list, this.listSize)

        Log.d("list", this.generatedList.toString())
        this.adapter.notifyDataSetChanged()
        this.lv.adapter = this.adapter

    }

}