package com.example.recyclerviewcolorsk

import android.graphics.Color
import kotlin.random.Random

class ColorHelper {
    public fun getColor(): Int {
        val rand = Random
        var c = Color.argb(255, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256))
        return c
    }
}