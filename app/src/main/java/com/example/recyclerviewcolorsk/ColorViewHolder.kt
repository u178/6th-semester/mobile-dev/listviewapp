package com.example.recyclerviewcolorsk

import android.graphics.Color
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class ColorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    // получаем ссылку на текстовое поле в каждом элементе списка
    val tv = itemView.findViewById<TextView>(R.id.color)
    val ch = ColorHelper()
    init {
        tv.setOnClickListener { _ ->
            Toast.makeText(tv.context, tv.text, Toast.LENGTH_SHORT).show()
            val c = ch.getColor();
            tv.text = itemView.context.getString(R.string.template, c)
            tv.setBackgroundColor(c)
        }

    }

    fun bindTo(color: Int) {
        tv.setBackgroundColor(color)
        // вывод кода цвета в 16-ричном виде
        tv.text = itemView.context.getString(R.string.template, color)
    }
}
